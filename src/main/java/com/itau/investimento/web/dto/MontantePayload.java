package com.itau.investimento.web.dto;

import com.itau.investimento.persistence.Aplicacao;


public class MontantePayload {
    private int id;
    private float valor;
    private int quantidadeMeses;
    private float montante;

    public MontantePayload(){}

    public MontantePayload(Aplicacao aplicacao){
        id = aplicacao.getId();
        valor = aplicacao.getValor();
        quantidadeMeses = aplicacao.getQuantidadeMeses();
        montante = aplicacao.getMontante();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

    public int getQuantidadeMeses() {
        return quantidadeMeses;
    }

    public void setQuantidadeMeses(int quantidadeMeses) {
        this.quantidadeMeses = quantidadeMeses;
    }

    public float getMontante() {
        return montante;
    }

    public void setMontante(float montante) {
        this.montante = montante;
    }

    public Aplicacao buildEntity(){
        Aplicacao aplicacao = new Aplicacao();
        aplicacao.setId(id);
        aplicacao.setValor(valor);
        aplicacao.setQuantidadeMeses(quantidadeMeses);
        aplicacao.setMontante(montante);

        return aplicacao;
    }
}
