package com.itau.investimento.web;

import com.itau.investimento.persistence.Aplicacao;
import com.itau.investimento.services.MontanteService;
import com.itau.investimento.web.dto.MontantePayload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class InvestimentoController {
    private MontanteService montanteService;

    @Autowired
    public InvestimentoController(MontanteService montanteService) {
            this.montanteService = montanteService;
    }

   @GetMapping("/investimento")
     public List<Aplicacao> getAll(){return montanteService.getAll();
    }

    @PostMapping("/investimento")
    public MontantePayload
        create(@RequestBody MontantePayload payload) {
        Aplicacao aplicacao = payload.buildEntity();
        return new MontantePayload(montanteService.create(aplicacao));
    }
}


