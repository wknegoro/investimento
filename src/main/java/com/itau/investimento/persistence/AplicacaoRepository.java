package com.itau.investimento.persistence;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AplicacaoRepository extends CrudRepository<Aplicacao, Integer> {
    List<Aplicacao> findAll();
}

