package com.itau.investimento.services;

import com.itau.investimento.persistence.Aplicacao;
import com.itau.investimento.persistence.AplicacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MontanteService {
    @Autowired
    private AplicacaoRepository aplicacaoRepository;
    public MontanteService(AplicacaoRepository aplicacaoRepository) {
           this.aplicacaoRepository = aplicacaoRepository;
    }
    public  List<Aplicacao> getAll(){
           return aplicacaoRepository.findAll();
    }

    public Aplicacao create(Aplicacao aplicacao){
        return aplicacaoRepository.save(aplicacao);
    }
}